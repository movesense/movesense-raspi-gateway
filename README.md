DEPRECATED

Much has changed since we brought up this "sensor firmware > Raspi python > IBM Cloud" invention

Better to look for tips here: 

[https://bitbucket.org/suunto/movesense-raspi-python3](https://bitbucket.org/suunto/movesense-raspi-python3/)


## Overview

This is Movesense BLE advertisement message gateway to IBM Watson IoT Cloud for Raspbian.


## Dependencies

* Python 2.7
* [PyBluez](https://github.com/pybluez/pybluez)
* [IBM Watson IoT Platform for Python](http://ibm-watson-iot.github.io/iot-python/)
* [IBM Cloud and Movesense broadcasting firmware](IBMdocumentation/README.md)


## Installation

Open terminal on Raspbian, then follow instructions by typing commands to terminal.

* Install needed libs
    `sudo apt-get install python-pip python-dev libbluetooth-dev libcap2-bin`
    `sudo pip install pybluez`

* Install IBM Watson IoT Platform
    `sudo pip install ibmiotf`

* Clone movesense-raspi-gateway repository
    `git clone https://bitbucket.org/suunto/movesense-raspi-gateway`

* Enter to movesense-raspi-gateway's setup directory
    `cd movesense-raspi-gateway/setup/`

* On setup directory, run installation script:
    `sudo ./install.sh`


## Post-install

There's need to deliver correct options for IBM Watson IoT Platform, but unfortunaly haven't succeed to do that without modifying IBM's code.
By patching application.py solves the problem, just run command below in setup directory:
    `sudo patch -d/ -p0 < ibmiotf_application.patch`
And when file name asked, give file name for patching:
    `/usr/local/lib/python2.7/dist-packages/ibmiotf/application.py`
File /usr/local/lib/python2.7/dist-packages/ibmiotf/application.py will be patched.


## Configuration

Movesense-raspi-gateway's configuration file will be write by install.sh to /etc/blegateway.cnf
Define configuration with your favorit editor.


## Running

After installation and configuration, gateway can be started and stoped by:
    `sudo service blegateway start`
    `sudo service blegateway stop`
