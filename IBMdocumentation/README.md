## Overview

This part describes how to set up the IBM Cloud for the Movesense-raspi-gateway.


## Dependencies

Registered IBM account is needed to connect Movesense to the [IBM Cloud](https://www.ibm.com/cloud/)


## Configuration

Navigate to [IBM Cloud/catalog](https://cloud.ibm.com/catalog) and pick an IOTF boilerplate:

![Alt text](iotf.png)


* Fill in the details and choose a suitable pricing plan

* Go IBM Cloud *Resource List* and open your brand-new Cloud Foundry App

* Choose the connected *your-iotf-service* and Launch it

* At *Devices* go to Device Types and +Add Device Type

* Choose Gateway and name it

* Browse back to Devices and +Add Device

* Choose previously created gateway, name it and remember to choose “Privileged Gateway” in permissions

* Give Authentication Token for the device, this will be used on Raspberry ( /etc/blegateway.cnf)

* Browse to IBM Watson IoT Platform / Apps and +Generate API Key, this will be used on Node-RED ibmiot-node

From IBM Cloud /Dashboard /Cloud Foundry Apps you can start Node-RED editor by choosing *Visit App URL*

Here’s a basic code you can use as a starting point (Node-RED / Import / Clipboard)

```
[{"id":"cdc1dcd6.327f4","type":"ibmiot in","z":"deb0d57.1c46528","authentication":"apiKey","apiKey":"60dc8d4f.38b944","inputType":"evt","logicalInterface":"","ruleId":"","deviceId":"","applicationId":"","deviceType":"+","eventType":"+","commandType":"","format":"json","name":"Movesense IoT","service":"registered","allDevices":false,"allApplications":"","allDeviceTypes":true,"allLogicalInterfaces":false,"allEvents":true,"allCommands":"","allFormats":false,"qos":0,"x":290,"y":385,"wires":[["7af6b5a5.15800c","20c88105.6f0336"]]},{"id":"25b676fa.c691a2","type":"deduplicate","z":"deb0d57.1c46528","name":"","keyproperty":"","expiry":"5","x":735,"y":490,"wires":[["31cc93f8.ef6bbc"],[]]},{"id":"24250e65.1beafa","type":"base64","z":"deb0d57.1c46528","name":"","action":"","property":"payload","x":725,"y":385,"wires":[["27b2c6e1.37e00a"]]},{"id":"7af6b5a5.15800c","type":"function","z":"deb0d57.1c46528","name":"data only","func":"\n    msg.payload = msg.payload.data;\n    return msg;","outputs":"1","noerr":0,"x":515,"y":385,"wires":[["24250e65.1beafa"]]},{"id":"27b2c6e1.37e00a","type":"function","z":"deb0d57.1c46528","name":"Movesense advertiser?","func":"\nvar myArray = msg.payload;\n\n// position20 0xfe = 254\nif(myArray[20] == 254){\n    \n    return msg;\n}\nelse {\n\n    return null;\n}","outputs":"1","noerr":0,"x":985,"y":385,"wires":[["79e1e65a.8b84f8"]]},{"id":"79e1e65a.8b84f8","type":"function","z":"deb0d57.1c46528","name":"RasPi parser","func":"function getFloat32(vec, i) {\n\n    var buf = new ArrayBuffer(4);\n    var view = new DataView(buf);\n    view.setUint8(0, vec[i + 3]);\n    view.setUint8(1, vec[i + 2]);\n    view.setUint8(2, vec[i + 1]);\n    view.setUint8(3, vec[i + 0]);\n    return view.getFloat32(0);\n}\n\nfunction getUint32(vec, i) {\n\n    var buf = new ArrayBuffer(4);\n    var view = new DataView(buf);\n    view.setUint8(0, vec[i + 3]);\n    view.setUint8(1, vec[i + 2]);\n    view.setUint8(2, vec[i + 1]);\n    view.setUint8(3, vec[i + 0]);\n    return view.getUint32(0);\n}\n\nfunction getUint16(vec, i) {\n    var buf = new ArrayBuffer(2);\n    var view = new DataView(buf);\n    view.setUint8(0, vec[i + 1]);\n    view.setUint8(1, vec[i + 0]);\n    return view.getUint16(0);\n}\n\nvar msgId = getUint32(msg.payload, 26);\nvar act = getFloat32(msg.payload, 30);\nvar hr = getFloat32(msg.payload, 34);\nvar rssi = getUint16(msg.payload, 38) // value-256=dB\n\nvar v3 = getUint16(msg.payload, 7)\nvar v4 = getUint32(msg.payload, 9)\n\nvar mac = v4 * 65536 + v3;\n\nmsg.payload = {\"ACT\": act, \"HR\": hr, \"MAC\": mac, \"msgID\": msgId, \"RSSI\":rssi};\nreturn msg;","outputs":1,"noerr":0,"x":525,"y":490,"wires":[["25b676fa.c691a2"]]},{"id":"20c88105.6f0336","type":"frequency","z":"deb0d57.1c46528","name":"freq1","interval":"5000","ntfyinterval":"12","x":505,"y":315,"wires":[[]]},{"id":"31cc93f8.ef6bbc","type":"debug","z":"deb0d57.1c46528","name":"","active":true,"tosidebar":true,"console":false,"tostatus":false,"complete":"true","x":925,"y":490,"wires":[]},{"id":"60dc8d4f.38b944","type":"ibmiot","z":"","name":"API Key for RasPi","keepalive":"60","serverName":"","cleansession":true,"appId":"","shared":false}]
```


## Movesense firmware

A reference Movesense sensor code can be found [here](https://bitbucket.org/suunto/movesense-device-lib/src/137d79457ead46f0154c701a429735abb7aebdf0/samples/activity_broadcast_app/?at=wip/timppis/activity-broadcast-app)


## Data format

The Raspberrypi gateway is agnostic to the data sent i.e. it's just passing the binary packets further.
A custom data format of the custom BLE advertisement message is defined on the sensor firmware and decoded again on Node-RED.
The received data packets look in this example like this:

```
27.2.2019   14.25.12 
iot-2/type/g/id/MSGW-007/evt/msg/fmt/json : msg : Object
object
topic: "iot-2/type/g/id/MSGW-007/evt/msg/fmt/json"
payload: object

ACT: 600.0024851921480149	'xyz' sum intensity in 1sec window
HR: 62.20000076293945		heart rate (bpm)
MAC: 12345678901234			MAC address of the sensor
msgID: 74					running message ID (Uint32)
RSSI: 191					RSSI value (191-256= -65dB)

deviceId: "MSGW-007"
deviceType: "g"
eventType: "msg"
format: "json"
```

## Help

Let the Movesense community help at [Movesense stackoverflow](https://stackoverflow.com/search?q=movesense)

## iOS gateway 

Recipe for Movesense iPhone gateway can be found [here](https://developer.ibm.com/recipes/tutorials/connect-a-suunto-movesense-sensor-to-ibm-cloud/)
