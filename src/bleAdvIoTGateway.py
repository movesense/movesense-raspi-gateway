import os
import sys
import time
import datetime
import argparse
import ConfigParser
import io
import json
from bleAdvScanner import bleAdvScanner
import ibmiotf.application
import uuid
import base64

client = None
target = None

def myCommandCallback(cmd):
    print 'Got:', cmd.event, cmd, type(cmd)
    #payload = json.loads(cmd.payload)
    #command = payload["command"]
    #print command


def callback(raw):
    global client
    global target
    payload = {'data':base64.b64encode(raw)}
    if not client.publishEvent("g", target, "msg", "json", payload):
         time.sleep(5)

def _main(argv):
    global client
    global target

    parser = argparse.ArgumentParser()
    parser.add_argument('-f', '--settingsFile', default='/etc/blegateway.cnf', help='Settings file (overrides all other parameters)')
    args = parser.parse_args()

    if args.settingsFile and os.path.exists(args.settingsFile):
        with open(args.settingsFile) as f:
            setf = f.read()
            config = ConfigParser.RawConfigParser(allow_no_value=True)
            config.readfp(io.BytesIO(setf))
    else:
        print 'Cannot read settings from "%s"' % args.settingsFile
        return

    bthci = config.get('bluetooth', 'hci')
    target = config.get('application', 'id').split(':')[1]

    try:
        options = ibmiotf.application.ParseConfigFile(args.settingsFile)
        client = ibmiotf.application.Client(options)
        client.connect()
        client.deviceEventCallback = myCommandCallback
    except ibmiotf.ConnectionException  as e:
        print e

    scanner = bleAdvScanner(callback, bt_device_id=int(bthci))
    scanner.start()

    # Run forever
    while (1):
        time.sleep(60)
        print "I'm alive"

    scanner.stop()
    db.close()

if __name__ == '__main__':
    _main(sys.argv[1:])

